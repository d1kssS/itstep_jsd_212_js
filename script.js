// Тип данных, Переменные
// Тип данных - какой-то вид информации(продукта)
// Примитивные и Ссылочные

// 1. Number - 1, 20, 20.5, 0.3, NaN (Not A Number)
// 2. String - 'text', "asd", `test2`
// 3. Boolean - true, false

// typeof - выводит тип данных значения

// console.log(typeof('205'))

// console.log(20 + 5)
// console.log(20 - 5)
// console.log(20 * 5)
// console.log(20 / 5)

// console.log(10)
// console.log(typeof 10.5)

// console.log(20 / 3)
// console.log(20 % 3)
// console.log((10 + 15) * 2)

// Переменная - ячейка памяти.

// var, let, const
// (var/let/const) (название переменной)

// let name; // создается - инициализируется

// name = 'Alex'; // - присваиваем значение

// let age = 30; // создается и присвается значение

// console.log(name, age)


// let num1 = 10;
// let num2 = 15;
// let num3 = num1 + num2;

// console.log('Сумма двух чисел', num3)

// var name = 'test';
// name = 'new'

// var name = 'Paul'

// console.log(name)

// var config = 'PASS1237812k12'

// console.log(config)

// var config = 21045

// let text = 'test'
// text = 123123

// console.log(text)

// const pi = 3.14;
// console.log(pi)

// const num = 0.6 + 0.7

// console.log(num.toFixed(5))

// конкатенация - соединение строк

// console.log(5 + '5')

// console.log('text'+'new')

// console.log('10' - 'asd')

// console.log(isNaN(Number('123')))
// isNaN - true - то значение NaN
// false - то это обычное число

// console.log(isFinite(1/0))
// isFinite - false - число бесконечное (infinity)
// true - обычное число

// console.log('number'/0)

// const discount = '10.5%'
// Int(integer) - целое - parseInt
// Float - вещественное, parseFloat
// parseFloat, parseInt - вытаскивают из строки числа

// console.log(parseFloat(discount))

// console.log(Math.floor(20.99))


const n = 7163789;

const num1 = n % 100;

console.log(num1)

const num2 = num1 / 10

console.log(Math.floor(num2))